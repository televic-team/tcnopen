Source: tcnopen
Section: libs
Priority: optional
Maintainer: Marc Leeman <marc.leeman@gmail.com>
Build-Depends: debhelper-compat (= 13),
                pkgconf,
                cmake,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/televic-team/tcnopen.git
Vcs-Browser: https://salsa.debian.org/televic-team/tcnopen
Homepage: https://www.tcnopen.org/

Package: libtcnopen0
Architecture: any
Depends:  ${shlibs:Depends},
          ${misc:Depends}
Description: TCNOpen Library for IEC61375 standardised communication
 TCN (Train Communication Network) is a series of international standards
 (IEC61375) developed by Working Group 43 of the IEC, specifying a
 communication system for the data communication within and between vehicles
 of a train. It is currently in use on many thousands of trains in the world
 in order to allow electronic devices to exchange information while operating
 aboard the same train.
 .
 TCNOpen is an open source initiative which the partner railway industries
 created with the aim to build in collaboration some key parts of new or
 upcoming railway standards, commonly known under the name TCN.
 .
 This package contains the shared runtime library.

Package: libtcnopen-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libtcnopen0 (= ${binary:Version})
Description: TCNOpen Library development files for IEC61375 communication
 TCN (Train Communication Network) is a series of international standards
 (IEC61375) developed by Working Group 43 of the IEC, specifying a
 communication system for the data communication within and between vehicles
 of a train. It is currently in use on many thousands of trains in the world
 in order to allow electronic devices to exchange information while operating
 aboard the same train.
 .
 TCNOpen is an open source initiative which the partner railway industries
 created with the aim to build in collaboration some key parts of new or
 upcoming railway standards, commonly known under the name TCN.
 .
 This package contains the static library and headers for development.
